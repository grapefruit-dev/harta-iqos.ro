let infoCI = '<div class=\"info\"> \n\
            <p>Insulele Canare</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>3</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, Bronze Label, Bronze Selection, Sienna Label, Sienna Selection, Turquoise Label, \
                        Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';

let infoAN = '<div class=\"info\"> \n\
            <p>Andorra</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p>TBA</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber, Blue, Bronze, Sienna, Turquoise, Yellow</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';

let infoAU = '<div class=\"info\"> \n\
            <p>Austria</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p>n/a</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p>n/a</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>n/a</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoBG = '<div class=\"info\"> \n\
            <p>Bulgaria</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>5.8</b> BGN/Leva </p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> carușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label Ambber Selection, Bronze Label, Bronze Selection, \
                        Sienna Label, Sienna Selection, Teak, Turquoise Label, Turquoise Selection,\
                        Yellow Label, Yellow Selection \
                        </p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoCR = '<div class=\"info\"> \n\
            <p>Croația</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>26</b> kuna(kn)</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Bronze Label, Bronze, Selection, Sienna Label, Sienna Selection, \
                        Turquoise Label, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoCY = '<div class=\"info\"> \n\
            <p>Cipru</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>4</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Bronze Label, Bronze Selection, Sienna Label, Sienna Selection, \
                        Silver Selection, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoCZ = '<div class=\"info\"> \n\
            <p>Republica Cehă</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>100</b> CZK/koruna (Kč)</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartuse de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, Bronze Label, Bronze Selection\
                        Sienna Label, Sienna Selection, Turquoise Label, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoDK = '<div class=\"info\"> \n\
            <p>Danemarca</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>44</b> DKK/krone(kr)</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Turquoise Label, Yellow Label, Amber Selection \
                        Blue Label, Blue Selectiom, Bronze Selection, Sienna Label\
                        Sienna Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoFR = '<div class=\"info\"> \n\
            <p>Franța</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>7</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, \
                        Bronze Label, Bronze Selection, Sienna Label, Sienna Selection,\
                        Turquoise Label, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoDE = '<div class=\"info\"> \n\
            <p>Germania</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>6</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Bronze Label, Broze Selection, Sienna Selection, \
                        Teak Selection, Terra Selection, Turquoise Label, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoGR = '<div class=\"info\"> \n\
            <p>Grecia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>4</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Bronze Label, Bronze Selection, Red Label\
                        Sienna Selection, Terra Selection, Turquoise Label, Turquoise Selection,\
                        Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoHU = '<div class=\"info\"> \n\
            <p>Ungaria</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>1,300.00</b> HUF/Ft</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, Broze Selection\
                        Sienna Selection, Turquoise Label, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoIT = '<div class=\"info\"> \n\
            <p>Italia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>4.5</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Selection, Bronze Label, Bronze Selection\
                        Sienna Label, Sienna Selection, Teak Selection, Turquoise Label, Turquoise Selection\
                        Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoLA = '<div class=\"info\"> \n\
            <p>Letonia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>3.5</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Blue Selection, Bronze Label, Bronze Selection, Sienna Selection, Silver Label\
                        Silver Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoLH = '<div class=\"info\"> \n\
            <p>Lituania</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>3.4</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Blue Selection, Bronze Selection, Sienna Selection, Silver Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoNL = '<div class=\"info\"> \n\
            <p>Olanda</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>6</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Blue Label, Blue Selection, Bronze Label, Bronze Selection, Sienna Label\
                        Sienna Selection, Turquoise Label, Turquouise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoPL = '<div class=\"info\"> \n\
            <p>Polonia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>14</b> PLN/zloty (zł)</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Blue Selection, Bronze Selection, Green Selection, Sienna Label 1.0, Sienna Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoPO = '<div class=\"info\"> \n\
            <p>Portugalia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>4.5</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Blue Label, Blue Selection, Bronze label, Sienna Label, Turquoise Label, Yellow Label</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoRO = '<div class=\"info\"> \n\
            <p>România</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>17</b> RON/lei</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, Bronze Label, Broze Selection, Sienna Label, Sienna Selection, \
                        Turuqoise Label, Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoSK = '<div class=\"info\"> \n\
            <p>Slovacia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>3.5</b> EUR/€</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, Bronze Label, Bronze Selection, Sienna Label, Sienna Selection, Turquoise Label\
                        Turquoise Selection, Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoSL = '<div class=\"info\"> \n\
            <p>Slovenia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>3.8</b> EUR / €</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Bronze Selection, Sienna Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoSP = '<div class=\"info\"> \n\
            <p>Spania</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>4.9</b> EUR / €</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Blue Label, Blue Selection, Bronze Selection, Sienna Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoSW = '<div class=\"info\"> \n\
            <p>Suedia</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>50</b> SEK/kronor</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Blue Selection, Sienna Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoCH = '<div class=\"info\"> \n\
            <p>Elveția</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>8</b> CHF/franc elvețian</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Selection, Bronze Selection, Sienna Label, Sienna Selection, Turquoise Selection, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';
let infoUK = '<div class=\"info\"> \n\
            <p>Marea Britanie</p> \n\
            <ul> \
                <li>\
                    <span>\
                        <img src="public/images/birthday.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Un pachet HEETS costă: </small>\
                        <p><b>5</b> GBP/£</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/money.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Poți lua cu tine din România: </small>\
                        <p><b>2</b> cartușe de HEETS</p>\
                    </div>\
                </li> \
                <li>\
                    <span>\
                        <img src="public/images/tobacco.png" alt="Birthday">\
                    </span>\
                    <div class="details">\
                        <small>Variante disponibile:</small>\
                        <p>Amber Label, Amber Selection, Blue Label, Blue Selection, Sienna Label, Sienna Selection, Turquoise Label, Turquoise Selection\
                        Yellow Label, Yellow Selection</p>\
                    </div>\
                </li> \
            </ul> \n\
            </div>\n\ ';

let $maparea2 = $("#world-map"),
    prevID = 'RO';

$maparea2.mapael({
    map: {
        name: "world_countries_mercator",
        zoom: {
            enabled: true,
            mousewheel: true,
            touch: true,
            maxLevel: 20,

            init: {
                latitude: 47,
                longitude: 15,
                level: 20
            },
        },

        afterInit: (container, paper, areas, plots) => {
            let arrayObjects = [];
            console.log(areas);
            console.log(plots);
            arrayObjects.push(plots);

            var select = document.createElement('select');
            for (var p = 0; p < arrayObjects.length; p++) {
                let x = arrayObjects[p];
                for (var prop in x) {
                    var option = document.createElement("option");
                    option.value = prop;
                    option.text = x[prop].options.country;
                    select.appendChild(option);
                }
            }
            document.getElementById('countries').appendChild(select).appendChild(option);

            $('select').change(function () {
                var selectedCountry = $(this).children("option:selected").val();

                $maparea2.trigger('zoom', {
                    plot: selectedCountry,
                    level: 20,
                });

                var oldData = {
                    'areas': {}
                };
                var newData = {
                    'areas': {}
                };

                oldData.areas[prevID] = {attrs: {fill: '#ffffff'}}
                newData.areas[selectedCountry] = {
                    attrs: {
                        fill: "#000"
                    }
                };

                $maparea2.trigger('update', [{mapOptions: oldData}]);
                $maparea2.trigger('update', [{mapOptions: newData}]);
                prevID = selectedCountry;
            });
        },

        defaultArea: {
            attrs: {
                fill: '#FFF',
                stroke: '#dcdcdb',
                'stroke-width': 0.2
            },
            attrsHover: '#333',
        },

        defaultPlot: {
            size: 30,

            eventHandlers: {
                click: function (e, id, mapElem, textElem, elemOptions) {
                    console.log(textElem, 'textElem');
                    console.log(id,'id');
                    if (typeof elemOptions.country != 'undefined') {
                        console.log(elemOptions.showInfo, 'country html info');
                        $('.country div').html(elemOptions.showInfo).css({
                            display: 'none'
                        }).fadeIn('slow');
                    }
                    // $('#gftDev #iqosMap .col-2 .country > div .info ul > li:nth-of-type(3) .details p').css({fontSize: '18px', lineHeight: '25px'});
                    $maparea2.trigger('zoom', {plot: id});

                    var oldData = {
                        'areas': {},
                    };

                    var newData = {'areas': {}};

                    oldData.areas[prevID] = {attrs: {fill: '#ffffff'}}

                    newData.areas[id] = {
                        attrs: {
                            fill: "#000"
                        }
                    };
                    $maparea2.trigger('update', [{mapOptions: oldData}]);
                    $maparea2.trigger('update', [{mapOptions: newData}]);
                    prevID = id;
                },
            },
        }
    },

    legend: {
        plot: {
            slices: [{
                type: 'image',
                width: 6,
                height: 6,
                url: 'public/images/pin.svg',
                value: 1,
                fill: 'red',

                attrsHover: {
                    cursor: 'pointer'
                }
            }],
        }
    },

    defaultArea: {
        attrs: {
            fill: "#d09a59",
            stroke: "#f4f4e8",
        },

        text: {
            content: 'Germania',
            position: 'bottom',
            'font-size': 8
        },
    },

    plots: {
        'default': {
            latitude: 52.25,
            longitude: 13.6,
            level: 20,
            value: 2,
            country: 'Alege o țară',
            showInfo: 'defaultMessage'
        },

        'AT': {
            latitude: 47.51,
            longitude: 14.55,
            value: 1,
            country: 'Austria',
            fill: '#000',
            showInfo: infoAU,
            text: {
                content: "Austria",
                position: "top",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 2
            },
        },

        'AN': {
            latitude: 42.30,
            longitude: 1.31,
            value: 1,
            country: 'Andorra',
            fill: '#000',
            showInfo: infoAN,
            text: {
                content: "Andorra",
                position: "top",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 2
            },
        },

        'BG': {
            latitude: 42.41,
            longitude: 23.19,
            value: 1,
            country: 'Bulgaria',
            fill: '#000',
            showInfo: infoBG,
            text: {
                content: "Bulgaria",
                position: "bottom",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 3
            },
        },

        'HR': {
            latitude: 44.10,
            longitude: 15.30,
            value: 1,
            country: 'Croatia',
            fill: '#000',
            showInfo: infoCR,
            text: {
                content: "Croatia",
                position: "bottom",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 2
            },
        },

        'CY': {
            latitude: 35,
            longitude: 32,
            value: 1,
            country: 'Cipru',
            fill: '#000',
            showInfo: infoCY,
            text: {
                content: "Cipru",
                position: "bottom",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 3
            },
        },

        'CZ': {
            latitude: 49.81,
            longitude: 15.47,
            value: 1,
            country: 'Republica Ceha',
            fill: '#000',
            showInfo: infoCZ,
            text: {
                content: "Republica Ceha",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'DK': {
            latitude: 56.26,
            longitude: 9.50,
            value: 1,
            country: 'Danemarca',
            fill: '#000',
            showInfo: infoDK,
            text: {
                content: "Danemarca",
                position: "bottom",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 3
            },
        },

        'FR': {
            latitude: 48.86,
            longitude: 2.3444,
            value: 1,
            country: 'Franta',
            fill: '#000',
            showInfo: infoFR,
            text: {
                content: "Franta",
                position: "bottom",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 3
            },
        },

        'RO': {
            latitude: 44.25,
            longitude: 26.6,
            value: 1,
            size: 3,
            country: 'România',
            fill: '#000',
            showInfo: infoRO,
            text: {
                content: "România",
                position: "top",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 3},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 4
            },
        },

        'DE': {
            latitude: 52.25,
            longitude: 13.6,
            value: 1,
            size: 3,
            country: "Germania",
            showInfo: infoDE,
            text: {
                content: "Germania",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'GR': {
            latitude: 39.07,
            longitude: 21.82,
            value: 1,
            size: 3,
            country: "Grecia",
            showInfo: infoGR,
            text: {
                content: "Grecia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'HU': {
            latitude: 47.16,
            longitude: 19.50,
            value: 1,
            size: 3,
            country: "Ungaria",
            showInfo: infoHU,
            text: {
                content: "Ungaria",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        '': {
            latitude: 28,
            longitude: -16,
            value: 1,
            size: 3,
            country: 'Insulele Canare',
            showInfo: infoCI,
            text: {
                content: "Insulele Canare",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            }
        },

        'IT': {
            latitude: 41.87,
            longitude: 12.56,
            value: 1,
            size: 3,
            country: "Italia",
            showInfo: infoIT,
            text: {
                content: "Italia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'LV': {
            latitude: 56.87,
            longitude: 24.60,
            value: 1,
            size: 3,
            country: "Letonia",
            showInfo: infoLA,
            text: {
                content: "Letonia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'LT': {
            latitude: 55.1694,
            longitude: 23.8813,
            value: 1,
            size: 3,
            country: "Lituania",
            showInfo: infoLH,
            text: {
                content: "Lituania",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'NL': {
            latitude: 52.13,
            longitude: 5.29,
            value: 1,
            size: 3,
            country: "Olanda",
            showInfo: infoNL,
            text: {
                content: "Olanda",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'PL': {
            latitude: 51.91,
            longitude: 19.14,
            value: 1,
            size: 3,
            country: "Polonia",
            showInfo: infoPL,
            text: {
                content: "Polonia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'PT': {
            latitude: 39.39,
            longitude: -8.22,
            value: 1,
            size: 3,
            country: "Portugalia",
            showInfo: infoPO,
            text: {
                content: "Portugalia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'SK': {
            latitude: 48.66,
            longitude: 19.69,
            value: 1,
            size: 3,
            country: "Slovacia",
            showInfo: infoSK,
            text: {
                content: "Slovacia",
                position: "right",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'SI': {
            latitude: 46.15,
            longitude: 14.99,
            value: 1,
            size: 3,
            country: "Slovenia",
            showInfo: infoSL,
            text: {
                content: "Slovenia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'ES': {
            latitude: 40.46,
            longitude: -2.74,
            value: 1,
            size: 3,
            country: "Spania",
            showInfo: infoSP,
            text: {
                content: "Spania",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'SE': {
            latitude: 60.12,
            longitude: 18.64,
            value: 1,
            size: 3,
            country: "Suedia",
            showInfo: infoSW,
            text: {
                content: "Suedia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'CH': {
            latitude: 46.81,
            longitude: 8.22,
            value: 1,
            size: 3,
            country: "Elvetia",
            showInfo: infoCH,
            text: {
                content: "Elvetia",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },

        'GB': {
            latitude: 55.37,
            longitude: -3.43,
            value: 1,
            size: 3,
            country: "Marea Britanie",
            showInfo: infoUK,
            text: {
                content: "Marea Britanie",
                position: "left",
                attrs: {"font-size": 2, fill: "#D09A59", opacity: 1},
                attrsHover: {fill: "#D09A59", opacity: 1},
                margin: 1
            },
        },
    },
});

//# sourceMappingURL=app.js.map
